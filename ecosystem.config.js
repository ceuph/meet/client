module.exports = {
  apps: [{
    name: 'meet-client',
    script: 'serve',
    env: {
      PM2_SERVE_PATH: 'dist/spa',
      PM2_SERVE_PORT: 8080,
      PM2_SERVE_SPA: 'true',
      PM2_SERVE_HOMEPAGE: '/index.html',
    },
  }],
};
