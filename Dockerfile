FROM keymetrics/pm2:14-buster

# Bundle APP files
COPY dist/ dist/
COPY ecosystem.config.js ecosystem.config.js

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]
