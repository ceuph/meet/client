import { boot } from 'quasar/wrappers';
import axios, { AxiosInstance } from 'axios';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
  }
}

type ApiListener = (event: string, data?: object | string | number) => void;
type ApiEvent = 'loading' | 'ready' | 'authenticated' | 'logout';

class Api {
  private state: ApiEvent = 'loading';

  // eslint-disable-next-line no-use-before-define
  private static instance: Api;

  private baseUrl: string | null = null;

  private apiUrl: string | null = null;

  private api: AxiosInstance | null = null;

  private authenticatedApi: AxiosInstance | null = null;

  private token: string | null = null;

  private listeners: Map<ApiListener, ApiListener> = new Map<ApiListener, ApiListener>();

  public static factory() {
    if (this.instance) {
      return this.instance;
    }
    this.instance = new Api();
    return this.instance;
  }

  public async boot(baseUrl: string) {
    this.setBaseUrl(baseUrl);
    await this.getApi();
    this.state = 'ready';
    this.triggerListeners();
  }

  public async getApi() {
    if (this.apiUrl && this.api) {
      return this.api;
    }
    const baseUrl = await this.getBaseUrl();
    const response = await axios.get(`${baseUrl}/config.json`);
    this.setApiUrl(response.data.API_URL);
    this.api = axios.create({
      baseURL: await this.getApiUrl(),
    });
    return this.api;
  }

  public async getAuthenticatedApi(): Promise<AxiosInstance | null> {
    if (this.authenticatedApi) {
      return this.authenticatedApi;
    }

    if (this.isAuthenticated()) {
      await this.getApi();
      this.authenticatedApi = axios.create({
        baseURL: await this.getApiUrl(),
        headers: { Authorization: `Bearer ${this.getToken()}` },
      });
    }

    return this.authenticatedApi;
  }

  public async authenticate(params: object) {
    const api = await this.getApi();
    const response = await api.get('/google/callback', {
      params,
    });
    this.setToken(response.data.token);
    const authenticatedApi = await this.getAuthenticatedApi();
    this.triggerListeners('authenticated');
    return authenticatedApi;
  }

  public logout() {
    this.token = null;
    localStorage.clear();
    this.state = 'logout';
    this.triggerListeners();
    this.state = 'ready';
    this.triggerListeners();
  }

  public setBaseUrl(baseUrl: string): Api {
    this.baseUrl = baseUrl;
    return this;
  }

  public async getBaseUrl(): Promise<string> {
    return this.getUrl('baseUrl');
  }

  public setApiUrl(apiUrl: string): Api {
    this.apiUrl = apiUrl;
    return this;
  }

  public async getApiUrl(): Promise<string> {
    return this.getUrl('apiUrl');
  }

  public isAuthenticated() {
    return this.getToken() !== null;
  }

  private setToken(token: string): Api {
    localStorage.setItem('token', token);
    this.token = token;
    return this;
  }

  private getToken(): string | null {
    if (this.token !== null) {
      return this.token;
    }
    this.token = localStorage.getItem('token');
    return this.token;
  }

  private async getUrl(urlProp: 'baseUrl' | 'apiUrl'): Promise<string> {
    if (this[urlProp] !== null) {
      return this[urlProp] ?? '';
    }
    await new Promise((resolve, reject) => {
      let counter = 0;
      const i = setInterval(() => {
        if (this[urlProp] !== null) {
          resolve(this[urlProp]);
          clearInterval(i);
        }
        counter += 1;
        if (counter >= 50) {
          reject(`Api ${urlProp} is taking too long to read.`);
        }
      }, 100);
    });
    return this[urlProp] ?? '';
  }

  public addListner(listener: ApiListener) {
    this.listeners.set(listener, listener);
    listener(this.state);
  }

  public removeListener(listener: ApiListener) {
    this.listeners.delete(listener);
  }

  private triggerListeners(data?: object | string | number) {
    this.listeners.forEach((listener) => {
      listener(this.state, data);
    });
  }
}

const api = Api.factory();

export default boot(({ app, router }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api;
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
  api.boot(router.options.history.base);
});

export { api };
