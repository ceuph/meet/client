import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/google',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: 'google-callback', path: 'callback', component: () => import('pages/Google/CallbackPage.vue') },
      { name: 'google-logout', path: 'logout', component: () => import('pages/Google/LogoutPage.vue') },
    ],
  },

  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: 'home', path: '', component: () => import('pages/IndexPage.vue') },
      { name: 'new-room', path: 'new', component: () => import('pages/RoomFormPage.vue') },
      { name: 'edit-room', path: 'edit', component: () => import('pages/RoomFormPage.vue') },
      { name: 'room', path: 'room/:id', component: () => import('pages/RoomPage.vue') },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
