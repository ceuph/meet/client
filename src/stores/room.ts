import { AxiosResponse } from 'axios';
import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';

export interface Room {
  id?: string,
  name: string,
  description: string,
  access: 'CEU' | 'PUBLIC' | 'PRIVATE',
  start_at: Date,
  end_at: Date,
  domain?: string,
}

export interface RoomJwt {
  jwt: string,
  room: Room,
}

export const useRoomStore = defineStore('room', {
  state: () => ({
    rooms: [] as Array<Room>,
    room: null as Room | null,
    error: false as string | boolean,
  }),

  actions: {
    async create(data: Room) {
      const authApi = await api.getAuthenticatedApi();
      if (authApi) {
        const response = await authApi.post<Room>('/rooms', data);
        return response;
      }
      throw new Error('Auth API not ready.');
    },
    async update(data: Room) {
      const authApi = await api.getAuthenticatedApi();
      if (authApi) {
        const response = await authApi.patch<Room>(`/rooms/${data.id}`, data);
        return response;
      }
      throw new Error('Auth API not ready.');
    },
    async find(id: string): Promise<AxiosResponse<Room>> {
      const authApi = await api.getAuthenticatedApi();
      if (authApi) {
        const response = await authApi.get<Room>(`/rooms/${id}`);
        return response;
      }
      throw new Error('Auth API not ready.');
    },
    async jwt(id: string): Promise<AxiosResponse<RoomJwt>> {
      const authApi = await api.getAuthenticatedApi();
      if (authApi) {
        const response = await authApi.get<RoomJwt>(`rooms/${id}/jwt`);
        return response;
      }
      throw new Error('Auth API not ready.');
    },
    refresh() {
      api.getAuthenticatedApi().then((authApi) => {
        if (authApi) {
          authApi.get<Array<Room>>('rooms').then((response) => {
            this.rooms = response.data;
          });
          return;
        }
        throw new Error('Not Authenticated');
      }).catch((err) => {
        this.error = err;
      });
    },
  },
});
